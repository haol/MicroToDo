#include "ClickableLabel.h"
#include <QJsonDocument>

QString ClickableLabel::token;

ClickableLabel::ClickableLabel(QWidget* parent): QLabel(parent){}

ClickableLabel::ClickableLabel(const QString& text, QWidget* parent)
    : QLabel(parent){
    this->setText(text);
    this->setMinimumHeight(100);
    this->setMaximumHeight(100);
}

void ClickableLabel::mouseReleaseEvent(QMouseEvent *ev){
    if(text().isEmpty() || id.isEmpty()){
        emit clicked();
    }else{
        isPressed = false;
        switchStatus();
    }
}

void ClickableLabel::mousePressEvent(QMouseEvent *event){
    isPressed = true;
    emit pressed();
}


void ClickableLabel::switchStatus(){
    req.setHeader("Authorization", token);
    QString jsonStr = completed ? "{\"status\":\"notStarted\"}" : "{\"status\":\"completed\"}";
    QJsonDocument json = QJsonDocument::fromJson(jsonStr.toLocal8Bit().data());
    req.Patch(QString("https://graph.microsoft.com/v1.0/me/todo/lists/%1/tasks/%2").arg(listID).arg(id), json.object());
    this->hide();
}
