#include "settings.h"
#include "ui_settings.h"
#include "microtodo.h"
#include <QCloseEvent>
#include <QSpinBox>

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    this->parent = parent;
    this->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setWindowModality(Qt::ApplicationModal);

    db = QSqlDatabase::database("MicroToDo_CONNECTION");
    db.setDatabaseName("MicroToDo.db");
    db.open();
    query = new QSqlQuery(db);

    ui->setupUi(this);

    query->exec("Select r,g,b,alpha,width,height from form");
    query->next();
    ui->bgR->setValue(query->value(0).toInt());
    ui->bgG->setValue(query->value(1).toInt());
    ui->bgB->setValue(query->value(2).toInt());
    ui->bgA->setValue(query->value(3).toInt());
    ui->widthSlider->setValue(query->value(4).toInt());
    ui->heightSlider->setValue(query->value(5).toInt());

    query->exec("Select name,size,r,g,b,bold,italic from fonts where use=1");
    query->next();
    ui->fontChoose->setCurrentFont(QFont(query->value(0).toString()));
    ui->fontSize->setValue(query->value(1).toInt());
    ui->fR->setValue(query->value(2).toInt());
    ui->fG->setValue(query->value(3).toInt());
    ui->fB->setValue(query->value(4).toInt());
    ui->isBold->setChecked(query->value(5).toBool());
    ui->isItalic->setChecked(query->value(6).toBool());

    connect(ui->Cancel, &QPushButton::clicked, this, &Settings::exit);
    connect(ui->bgR, SIGNAL(valueChanged(int)), this, SLOT(refreshBgColor()));
    connect(ui->bgG, SIGNAL(valueChanged(int)), this, SLOT(refreshBgColor()));
    connect(ui->bgB, SIGNAL(valueChanged(int)), this, SLOT(refreshBgColor()));
    connect(ui->bgA, SIGNAL(valueChanged(int)), this, SLOT(refreshBgColor()));
    connect(ui->fR, SIGNAL(valueChanged(int)), this, SLOT(refreshFontColor()));
    connect(ui->fG, SIGNAL(valueChanged(int)), this, SLOT(refreshFontColor()));
    connect(ui->fB, SIGNAL(valueChanged(int)), this, SLOT(refreshFontColor()));
    connect(ui->isBold, &QCheckBox::stateChanged, this, &Settings::refreshFontColor);
    connect(ui->isItalic, &QCheckBox::stateChanged, this, &Settings::refreshBgColor);
    connect(ui->fontChoose, &QFontComboBox::currentFontChanged, this, &Settings::refreshFontColor);
    connect(ui->saveBtn, SIGNAL(clicked()), this, SLOT(save()));
}

Settings::~Settings(){
    //parent->setEnabled(true);
    if(query) delete query;
    delete ui;
}

void Settings::closeEvent(QCloseEvent *e){
    query->exec("Select r,g,b,alpha from form");
    query->next();
    ((MicroToDo*)parent)->changeColor(query->value(0).toInt(), query->value(1).toInt(), query->value(2).toInt(), query->value(3).toInt(), false);

    query->exec("Select name,size,r,g,b,bold,italic from fonts where use=1");
    query->next();
    ((MicroToDo*)parent)->setFont(query->value(0).toString(), query->value(2).toInt(), query->value(3).toInt(), query->value(4).toInt(), query->value(1).toInt(), query->value(5).toBool(), query->value(6).toBool(), false);

    e->ignore();
    reject();
}

void Settings::refreshBgColor(){
    unsigned char r = ui->bgR->value();
    unsigned char g = ui->bgG->value();
    unsigned char b = ui->bgB->value();
    unsigned char a = ui->bgA->value();
    QString qss = QString("background-color: rgba(%1, %2, %3, %4);").arg(r).arg(g).arg(b).arg(a);
    ui->bgColorLabel->setStyleSheet(qss);
    ((MicroToDo*)parent)->changeColor(r, g, b ,a, false);
    refreshFontColor();
}

void Settings::refreshFontColor(){
    unsigned char r = ui->fR->value();
    unsigned char g = ui->fG->value();
    unsigned char b = ui->fB->value();
    QString qss = QString("background-color: rgb(%1, %2, %3);").arg(r).arg(g).arg(b);
    ui->fColorLabel->setStyleSheet(qss);
    ((MicroToDo*)parent)->setFont(ui->fontChoose->currentText(), r, g, b, ui->fontSize->value(), ui->isBold->isChecked(), ui->isItalic->isChecked(), false);
}

void Settings::save(){
    unsigned char r = ui->bgR->value();
    unsigned char g = ui->bgG->value();
    unsigned char b = ui->bgB->value();
    unsigned char a = ui->bgA->value();
    ((MicroToDo*)parent)->changeColor(r, g, b ,a, true);
    r = ui->fR->value();
    g = ui->fG->value();
    b = ui->fB->value();
    ((MicroToDo*)parent)->setFont(ui->fontChoose->currentText(), r, g, b, ui->fontSize->value(), ui->isBold->isChecked(), ui->isItalic->isChecked(), true);
    close();
}

void Settings::on_widthSlider_valueChanged(int value){
    parent->resize(QSize(value, parent->size().height()));
    ui->widthLabel->setText(QString::number(value));
}


void Settings::on_heightSlider_valueChanged(int value){
    parent->resize(QSize(parent->size().width(), value));
    ui->heightLabel->setText(QString::number(value));
}

