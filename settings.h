#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QSqlDatabase>

QT_BEGIN_NAMESPACE
namespace Ui {class Settings;}
QT_END_NAMESPACE

class Settings : public QDialog
{
    Q_OBJECT

public:
    Settings(QWidget *parent = nullptr);
    ~Settings();

protected slots:
    void exit(){close();}
    void refreshBgColor();
    void refreshFontColor();
    void save();

private slots:
    void on_widthSlider_valueChanged(int value);

    void on_heightSlider_valueChanged(int value);

private:
    QWidget* parent;
    Ui::Settings *ui;
    QSqlDatabase db;
    QSqlQuery* query = 0;

    void closeEvent(QCloseEvent *) override;
};

#endif // SETTINGS_H
