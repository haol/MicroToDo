#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include "HttpRequest.h"

class ClickableLabel : public QLabel
{
    Q_OBJECT
public:
    static QString token;
    static QString listID;
    bool completed = false;
    bool isPressed = false;
    QString id;
    explicit ClickableLabel(QWidget* parent = 0);
    explicit ClickableLabel(const QString& text = "", QWidget* parent = 0);

signals:
    void clicked();
    void pressed();

protected:
    void mouseReleaseEvent(QMouseEvent* event);
    //void mouseDoubleClickEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);

private:
    HttpRequest req;
    void switchStatus();
};

#endif // CLICKABLELABEL_H
