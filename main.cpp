#include "microtodo.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MicroToDo* w = new MicroToDo(&a);
    w->show();
    int re = a.exec();
    delete w;
    return re;
}
